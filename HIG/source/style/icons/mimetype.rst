MIME Type Icons
===============
MIME type icons are used to depict documents and files. They come in four sizes:
16px, 22px, 32px, and 64px. MIME type icons use the `monochrome style \
<index.html#monochrome-icon-style>`__ for 16px and 22px sizes, and the
`colorful style <index.html#colorful-icon-style>`__ for 32px and 64px sizes.

Monochrome MIME type icons
~~~~~~~~~~~~~~~~~~~~~~~~~~
Unlike other monochrome icons, Monochrome MIME type icons are drawn with the
primary color of the large colorful version rather than following the general
monochrome icon color guidelines:

.. image:: /img/Breeze-icon-design-mimetype-small.png
   :alt: Small monochrome document MIME types

Margins for monochrome MIME type icons are the same as most other monochrome icons:

.. figure:: /img/icon-margins-mimetype-monochrome.png
   :alt: Margins for monochrome MIME type icons

   Margins for 16px and 22px monochrome MIME type icons

Colorful MIME type icons
~~~~~~~~~~~~~~~~~~~~~~~~
Like Places icons, the colorful versions consist of the monochrome icon used as
a foreground symbol on top of a background. Margins are small, with only 2px on
the top and bottom for the 32px icons, and 3px on the top and bottom for the
64px icons:

.. figure:: /img/icon-margins-mimetype-32.png
   :alt: Margins for monochrome MIME type icons

   Margins for 32px monochrome MIME type icons

.. figure:: /img/icon-margins-mimetype-64.png
   :alt: Margins for monochrome MIME type icons

   Margins for 64px monochrome MIME type icons

The background style depends on the file type.

For archives, packages, compressed files, and disk images, the background is a
square with a zipper going halfway down:

.. image:: /img/Breeze-icon-design-mimetype-archive.png
   :alt: Large colorful archive MIME types

For images, the background is a horizontal rectangle with the top-right corner
folded over, and the fold casts a shadow:

.. image:: /img/Breeze-icon-design-mimetype-image.png
   :alt: Large colorful image MIME types

For video files, the background is a horizontal rectangle that looks like a
filmstrip:

.. image:: /img/Breeze-icon-design-mimetype-video.png
   :alt: Large colorful video MIME types

For audio files, the background is a CD sleeve with a CD partially visible:

.. image:: /img/Breeze-icon-design-mimetype-audio.png
   :alt: Large colorful video MIME types

For documents and everything else, the background is a vertical rectangle with
the top-right corner folded over, and the fold casts a shadow:

.. image:: /img/Breeze-icon-design-mimetype-document.png
   :alt: Large colorful document MIME types

As with the Places icons, the foreground symbol does not cast a shadow.
